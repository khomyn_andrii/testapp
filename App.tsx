import React from 'react';
import {StyleSheet, useColorScheme, View} from 'react-native';
import {MainScreen} from './app/screens';
import {Provider} from 'react-redux';
import {store} from './app/redux';

const App = () => {
  const isDark = useColorScheme() === 'dark';

  const backgroundStyles = {
    backgroundColor: isDark ? '#393939' : '#F1F1F1',
  };

  return (
    <Provider store={store}>
      <View style={[backgroundStyles, styles.container]}>
        <MainScreen />
      </View>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 14,
  },
});

export default App;

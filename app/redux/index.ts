import {configureStore} from '@reduxjs/toolkit';
import {mainStore} from './main';

export const store = configureStore({
  reducer: {
    main: mainStore.reducer,
  },
  devTools: true,
});

export type TStore = ReturnType<typeof store.getState>;

import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface IMainState {
  activeCurrency: Currencies;
  quantity: number;
}

export enum Currencies {
  USD = 'USD',
  CAD = 'CAD',
  EUR = 'EUR',
  UAH = 'UAH',
  GBP = 'GBP',
}

export const currencySymbols = {
  USD: '$',
  CAD: 'CA$',
  EUR: '€',
  UAH: '₴',
  GBP: '£',
};

export const mainStore = createSlice({
  name: 'main',
  initialState: {
    loading: false,
    activeCurrency: Currencies.USD,
    quantity: 1,
  } as IMainState,
  reducers: {
    setActiveCurrency: (
      state,
      action: PayloadAction<IMainState['activeCurrency']>,
    ) => {
      state.activeCurrency = action.payload;
    },
    setQuantity: (state, action: PayloadAction<IMainState['quantity']>) => {
      state.quantity = action.payload;
    },
  },
});

export const {setActiveCurrency, setQuantity} = mainStore.actions;

import React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';
import {store} from '../../../../redux';
import {QuantityInput} from '../index';

describe('MainScreen/QuantityInput', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <QuantityInput />
    </Provider>,
  );

  const element = tree.root.findByProps({testID: 'QuantityInput'});

  it('renders correctly', () => {
    expect(tree.toJSON()).toMatchSnapshot();
  });

  expect(element.props.value).toEqual('1');
});

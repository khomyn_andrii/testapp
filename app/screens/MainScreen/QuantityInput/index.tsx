import React from 'react';
import {Text, TextInput, View} from 'react-native';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {useDispatch} from 'react-redux';
import {setQuantity} from '../../../redux/main';
import {useStyles} from './styles';
import {useColorStyles} from '../styles';

const validationSchema = Yup.object({
  quantity: Yup.number().min(0).required().typeError('not valid quantity'),
});

export const QuantityInput = () => {
  const dispatch = useDispatch();
  const {
    textInputStyle,
    container,
    errorStyle,
    topLine,
    lineStyle,
  } = useStyles();
  const {textColor} = useColorStyles();

  return (
    <>
      <View style={[topLine, lineStyle]} />
      <Formik
        initialValues={{quantity: 1}}
        validationSchema={validationSchema}
        validateOnChange={true}
        onSubmit={values => {
          dispatch(setQuantity(values.quantity));
        }}>
        {({handleChange, handleBlur, handleSubmit, values, errors}) => (
          <View style={container}>
            <TextInput
              testID="QuantityInput"
              style={[textInputStyle, textColor]}
              autoFocus={true}
              onChangeText={handleChange('quantity')}
              onBlur={handleBlur('quantity')}
              value={values.quantity.toString()}
              keyboardType="numeric"
              onEndEditing={handleSubmit}
            />
            <Text style={errorStyle} numberOfLines={1}>
              {errors.quantity}
            </Text>
          </View>
        )}
      </Formik>
      <View style={lineStyle} />
    </>
  );
};

import {StyleSheet, useColorScheme} from 'react-native';

export const useStyles = () => {
  const isDark = useColorScheme() === 'dark';
  return StyleSheet.create({
    container: {
      alignItems: 'center',
      paddingTop: 30,
    },
    textInputStyle: {
      fontSize: 30,
      width: '100%',
      textAlign: 'center',
    },
    errorStyle: {
      height: 30,
      color: 'red',
    },
    lineStyle: {
      width: '100%',
      height: 2,
      backgroundColor: isDark ? 'white' : 'black',
    },
    topLine: {
      marginTop: 20,
    },
  });
};

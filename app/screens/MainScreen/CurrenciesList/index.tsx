import React from 'react';
import {View} from 'react-native';
import {Currencies} from '../../../redux/main';
import {Item} from './Item';
import {useStyles} from './styles';

export const CurrenciesList = () => {
  const {container} = useStyles();
  return (
    <View style={container}>
      {Object.values(Currencies).map(item => {
        return <Item key={`Currency_item_${item}`} currency={item} />;
      })}
    </View>
  );
};

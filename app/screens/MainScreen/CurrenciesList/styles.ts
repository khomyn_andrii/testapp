import {StyleSheet} from 'react-native';

export const useStyles = () => {
  return StyleSheet.create({
    container: {
      marginTop: 20,
    },
    itemContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      minHeight: 40,
    },
    flex: {
      flex: 1,
      textAlign: 'center',
      fontSize: 20,
    },
  });
};

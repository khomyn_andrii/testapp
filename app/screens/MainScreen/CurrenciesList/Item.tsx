import React, {useEffect, useState} from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import {useSelector} from 'react-redux';
import {TStore} from '../../../redux';
import {Currencies, currencySymbols} from '../../../redux/main';
import {useStyles} from './styles';

export const Item = ({currency}: {currency: Currencies}) => {
  const [loading, setLoading] = useState(true);
  const [value, setValue] = useState(1);
  const [error, setError] = useState(false);
  const {itemContainer, flex} = useStyles();

  const {activeCurrency, quantity} = useSelector<
    TStore,
    {
      quantity: TStore['main']['quantity'];
      activeCurrency: TStore['main']['activeCurrency'];
    }
  >(({main}) => ({
    quantity: main.quantity,
    activeCurrency: main.activeCurrency,
  }));

  const getData = async () => {
    try {
      const q = `${activeCurrency}_${currency}`;
      setLoading(true);
      setError(false);
      const jsonRes = await fetch(
        `https://free.currconv.com/api/v7/convert?q=${q}&compact=ultra&apiKey=bdbd50118e28345935e6`,
      );

      if (jsonRes.status === 200) {
        const res = await jsonRes.json();

        setValue(Number(res[q]));
      } else {
        setError(true);
      }
    } catch {
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getData();
  }, [activeCurrency]);

  if (activeCurrency === currency) {
    return null;
  }

  return (
    <View style={itemContainer}>
      <Text style={[flex]}>{currency}</Text>
      {error && <Text style={flex}>Error</Text>}
      {loading && <ActivityIndicator style={flex} />}
      {!error && !loading && (
        <Text style={flex}>{`${currencySymbols[currency]}${(
          value * quantity
        ).toFixed(4)}`}</Text>
      )}
    </View>
  );
};

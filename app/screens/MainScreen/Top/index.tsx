import React from 'react';
import {View, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {PickerList} from '../../../components';
import {TStore} from '../../../redux';
import {Currencies, setActiveCurrency} from '../../../redux/main';
import {useColorStyles} from '../styles';
import {useStyles} from './styles';

export const Top = () => {
  const {container, textStyle} = useStyles();
  const {textColor} = useColorStyles();
  const dispatch = useDispatch();
  const activeCurrency = useSelector<TStore, TStore['main']['activeCurrency']>(
    ({main}) => main.activeCurrency,
  );

  return (
    <View style={container}>
      <Text style={[textColor, textStyle]}>Rates for</Text>
      <PickerList
        onValueChange={v => dispatch(setActiveCurrency(v as Currencies))}
        activeValue={activeCurrency}
        list={Object.values(Currencies)}
      />
    </View>
  );
};

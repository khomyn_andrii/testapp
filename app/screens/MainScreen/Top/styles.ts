import {StyleSheet} from 'react-native';

export const useStyles = () => {
  return StyleSheet.create({
    container: {
      flexDirection: 'row',
      marginTop: 20,
      justifyContent: 'center',
      // alignItems: 'center',
    },
    textStyle: {
      fontSize: 20,
      marginRight: 10,
      alignSelf: 'center',
    },
  });
};

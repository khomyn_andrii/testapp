// __tests__/Intro-test.js
import React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';
import {store} from '../../../../redux';
import {Top} from '../index';

test('renders correctly MainScreen/Top', () => {
  const tree = renderer
    .create(
      <Provider store={store}>
        <Top />
      </Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

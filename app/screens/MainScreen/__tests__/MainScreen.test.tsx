// __tests__/Intro-test.js
import React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';
import {store} from '../../../redux';
import {MainScreen} from '../index';

test('renders correctly MainScreen', () => {
  const tree = renderer
    .create(
      <Provider store={store}>
        <MainScreen />
      </Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

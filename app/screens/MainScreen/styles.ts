import {StyleSheet, useColorScheme} from 'react-native';

export const useColorStyles = () => {
  const isDark = useColorScheme() === 'dark';

  return StyleSheet.create({
    textColor: {
      color: isDark ? 'white' : 'black',
    },
  });
};

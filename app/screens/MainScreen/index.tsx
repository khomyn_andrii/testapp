/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
import {CurrenciesList} from './CurrenciesList';
import {QuantityInput} from './QuantityInput';
import {Top} from './Top';

export const MainScreen = () => {
  return (
    <SafeAreaView>
      <ScrollView style={{minHeight: '100%'}}>
        <Top />
        <QuantityInput />
        <CurrenciesList />
      </ScrollView>
    </SafeAreaView>
  );
};

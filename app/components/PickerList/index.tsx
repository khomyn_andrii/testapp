import React from 'react';
import {Picker} from '@react-native-picker/picker';

export const PickerList = ({
  list,
  activeValue,
  onValueChange,
}: {
  list: string[];
  activeValue: string;
  onValueChange: (itemValue: string, itemIndex: number) => void;
}) => {
  return (
    <Picker
      selectedValue={activeValue}
      onValueChange={onValueChange}
      // eslint-disable-next-line react-native/no-inline-styles
      style={{flex: 1}}>
      {list.map(item => (
        <Picker.Item
          label={item}
          value={item}
          key={`Picker_${item}`}
          color="green"
        />
      ))}
    </Picker>
  );
};
